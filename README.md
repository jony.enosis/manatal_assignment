# How to run

1. Download and install Node.js - https://nodejs.org/en/download/
2. Clone the repository or download the source code
3. Go inside manatal_assignment folder and find the package.json file
4. Open cmd/terminal from this location
5. Execute following command from terminal: npm install
6. Execute following command from terminal to run the tests: npx cypress run



