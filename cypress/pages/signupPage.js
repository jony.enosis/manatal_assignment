class Signup{

    nameTextBoxSelector = '#name';
    organizationNameTextBoxSelector = '#organization_name';
    companyEmailAddressTextBoxSelector = '#company_email_address';
    confirmCompanyemailAddressTextBoxSelector = '#confirm_company_email_address';
    passwordTextBoxSelector = '#password';
    agreeCheckboxSelector = '#agree';
    submitButtonSelector = 'button[type="submit"]';
    confirmationEmailMessageSelector = '#confirmation-message-e2e-test';
    testEmailTextSelector = '#signup-app  b';
    mandatoryFieldsWarningMessageSelector = '#account_signup span';

}
export default Signup