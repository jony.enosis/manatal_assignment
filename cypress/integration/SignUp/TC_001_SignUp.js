import signup from '../../pages/signupPage.js'

const signupObj = new signup()
var base_url;

describe('User can signup successfully', function() {
    before(() =>
    {
        cy.fixture('signup_test_data.json').as('user_data')
    })
    it('Verify mandatory fields are behaving as expected and sign up functionality is working as expected', function() 
    {
        // Reading test data from fixtures - signup_test_data.json
        cy.get('@user_data').then((data) =>
        {
            // Reading baseUrl from cypress.json
            base_url = Cypress.env('baseUrl')
            cy.visit(base_url + 'signup/')

            // Check mandatory fields are behaving as expected
            cy.get(signupObj.submitButtonSelector).click()
            cy.get(signupObj.mandatoryFieldsWarningMessageSelector).should('contain', data.name_field_warning_message)
            cy.get(signupObj.mandatoryFieldsWarningMessageSelector).should('contain', data.company_email_field_warning_message)
            cy.get(signupObj.mandatoryFieldsWarningMessageSelector).should('contain', data.confirm_company_email_warning_message)
            cy.get(signupObj.mandatoryFieldsWarningMessageSelector).should('contain', data.password_field_warning_message)
            cy.get(signupObj.mandatoryFieldsWarningMessageSelector).should('contain', data.agree_checkbox_warning_message)
            cy.get(signupObj.mandatoryFieldsWarningMessageSelector).should('contain', data.organization_name_field_warning_message)

            cy.reload()
            // Validate sign up functionality with valid test data
            cy.get(signupObj.nameTextBoxSelector).type(data.name)
            cy.get(signupObj.organizationNameTextBoxSelector).type(data.organization_name)
            // Generate unique email address with first name, last name and append epoc time 
            let email = data.first_name + "." + data.last_name + "+" + Date.now() + "@candidate.manatal.com"
            cy.get(signupObj.companyEmailAddressTextBoxSelector).type(email)
            cy.get(signupObj.confirmCompanyemailAddressTextBoxSelector).type(email)
            cy.get(signupObj.passwordTextBoxSelector).type(data.password)
            cy.get(signupObj.agreeCheckboxSelector).click()
            cy.get(signupObj.submitButtonSelector).click()
            // Assertions - expected message and email
            cy.get(signupObj.confirmationEmailMessageSelector).should('contain', data.signup_confirmation_message)
            cy.get(signupObj.testEmailTextSelector).should('contain', email)
            
        })
    })
})